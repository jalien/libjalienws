#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class TJAlienDNSResolver;
#pragma link C++ class TJAlienConnectionManager;
#pragma link C++ class TJClientFile;
#endif
