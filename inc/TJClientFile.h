// Author: Nikola Hardi 3/6/2019
#ifndef ROOT_TJClientFile
#define ROOT_TJClientFile

#include <algorithm>
#include <cctype>
#include <cstring>
#include <iostream>
#include <locale>
#include <string>
#include <unistd.h>

#define __FILENAMEEXT__                                                        \
  (strrchr(__FILE__, '/') ? std::string(strrchr(__FILE__, '/') + 1)            \
                          : std::string(__FILE__))
#define __FILENAME__ __FILENAMEEXT__.substr(0, __FILENAMEEXT__.find('.'))
#define INFO(message)                                                          \
  std::cout << "\rInfo in <" << __FILENAME__ << "::" << __func__               \
            << ">: " << message << std::endl
#define ERROR(message)                                                         \
  std::cerr << "\rError in <" << __FILENAME__ << "::" << __func__              \
            << ">: " << message << std::endl

struct TJClientFile {
  TJClientFile(const char *filepath = NULL);
  bool loadFile(const char *filepath = NULL);
  std::string getDefaultPath();
  std::string getTmpdir();

  std::string sUsercert;
  std::string sUserkey;
  std::string fHost;
  std::string fHome;
  std::string fUser;
  std::string fPw;
  std::string tmpdir;
  std::string defaultJClientPath;
  int fPort;
  int fWSPort;
  bool isValid;

  // trim from start (in place)
  static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return !std::isspace(ch); }));
  }

  // trim from end (in place)
  static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return !std::isspace(ch); })
                .base(),
            s.end());
  }

  // trim from both ends
  static inline std::string trim(std::string s) {
    ltrim(s);
    rtrim(s);
    return s;
  }
};
#endif
