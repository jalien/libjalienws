#ifndef ROOT_TJAlienDNSResolver
#define ROOT_TJAlienDNSResolver

#include <algorithm>
#include <cstring>
#include <random>
#include <string>
#include <vector>

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && \
    !defined(__CLING__)
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#else
struct addrinfo;
#endif

using std::string;
using std::vector;

class TJAlienDNSResolver {
public:
  TJAlienDNSResolver(string host, int port);
  string get_next_addr();
  int length();
  void reset();

private:
  string host;
  string port;

  bool use_ipv6;
  int current_position;

  vector<string> addr_ipv4;
  vector<string> addr_ipv6;
  vector<string> addr_combined;
  string addr2string(const struct addrinfo *ai);
  vector<string> get_addr(const char *host, const char *port, int ipv = 6);
};

#endif
