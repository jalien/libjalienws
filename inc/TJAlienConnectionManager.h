// Author: Volodymyr Yurchenko 27/06/2019

#ifndef ROOT_TJAlienConnectionManager
#define ROOT_TJAlienConnectionManager

struct lws_context;
struct lws_context_creation_info;

#include "TJAlienCredentials.h"
#include "TJAlienDNSResolver.h"
#include "TJAlienSSLContext.h"
#include "TJClientFile.h"
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <unistd.h>

#define DEFAULT_JCENTRAL_SERVER "alice-jcentral.cern.ch"
#define UNUSED(x) (void)(x)

class TJAlienConnectionManager {
private:
  const int default_WSport = 8097;

  const std::string default_server = DEFAULT_JCENTRAL_SERVER;
  std::string fWSHost;   // websocket host
  int fWSPort;           // websocket port
  std::string sUsercert; // location of user certificate
  std::string sUserkey;  // location of user private key

  struct lws_context
      *context;    // Context contains all information about connection
  struct lws *wsi; // WebSocket Instance - real connection object, created
                   // basing on context

  void clearFlags();
  TJAlienCredentials creds;
  const int default_ping_timeout = 20;
  const int default_ping_interval = 20;

public:
  TJAlienConnectionManager() {} // default constructor
  ~TJAlienConnectionManager();
  int CreateConnection();
  void ConnectJBox(TJAlienCredentialsObject c);
  void ConnectJCentral(TJAlienCredentialsObject c,
                       string host = DEFAULT_JCENTRAL_SERVER,
                       string address = DEFAULT_JCENTRAL_SERVER);
  void MakeWebsocketConnection(TJAlienCredentialsObject creds, string host,
                               string address, int WSPort);
  void ForceRestart();
  std::string RunCommand(std::string command);
  bool IsConnected() const;
  std::string GetHost() { return fWSHost; };
  int GetPort() { return fWSPort; };
};
#endif
