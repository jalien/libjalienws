# JAliEn websocket interface library

This library manages the connections to ALICE JAliEn services through websocket interface. 

## API Overview

- Get websocket connection with default parameters
```
TJAlienConnectionManager connection;
int connection_mode = connection.CreateConnection();
```
`CreateConnection()` return codes:

| Code | Reason |
| ------ | ------ |
| 0 | connected to JBox |
| 1 | connected to JCentral with full grid certificate | 
| 2 | connected to JCentral with token certificate | 
| -1 | connection failed |

Client identity is loaded internally by the credentials manager (*libjalienO2*).

First of all the connection manager detects running JBox service and tries to connect to it. If this attempt has failed, the connection manager resolves known JCentral DNS address and connects to the available JCentral server.

- Connect only to JBox/JCentral
```
TJAlienCredentialsObject co = creds.get();
ConnectJBox(co);
// OR
ConnectJCentral(co, default_server, resolved_address);
```

- Set host and port variables in environment or in *$TMPDIR/jclient_token* file
```
JALIEN_WSPORT=10100
JALIEN_HOST=alice-jcentral.cern.ch
```

- Create websocket connection to a custom endpoint
```
MakeWebsocketConnection(co, host, resolved_address, ws_port);
```

- Resolve given server’s DNS name
```
TJAlienDNSResolver dns_jcentral(default_server, default_WSport);
for (int i = 0; i < dns_jcentral.length(); i++) {
  resolved_address = dns_jcentral.get_next_addr();
  ...
}
```

`get_next_addr()` loops over resolved IPv4 and IPv6 addresses in a randomized manner.

- Manage sending and receiving messages (commands)
```
readBuffer = connection.RunCommand(CreateJsonCommand(sCmd));
```

The connection manager is not aware of the message format. It sends and receives the message strings "as is". The client must take care of converting it's commands and server's responces into a proper format. JCentral's default format is JSON.
 
## Dependencies
- [libwebsockets](https://github.com/warmcat/libwebsockets)
- OpenSSL
- [libjalienO2](https://gitlab.cern.ch/jalien/libjalienO2)