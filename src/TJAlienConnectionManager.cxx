// @(#)root/net:$Id$
// Author: Volodymyr Yurchenko 27/06/2019

#include "TJAlienConnectionManager.h"
#include <iomanip>
#include <iostream>

#include "lws_config.h"
#include <libwebsockets.h>

static int gDebug = 0;

int destroy_flag = 0;
int connection_flag = 0;
int writeable_flag = 0;
int receive_flag = 0;
std::string readBuffer = "";

int ws_service_callback(struct lws *wsi, enum lws_callback_reasons reason,
                        void *user, void *in, size_t len);
int websocket_write_back(struct lws *wsi_in, const char *str, int str_size_in);
size_t WriteCallback(void *contents, size_t size, size_t nmemb);

//______________________________________________________________________________
TJAlienConnectionManager::~TJAlienConnectionManager() {
  if (context)
    lws_context_destroy(context);

  if (creds.has(cJOB_TOKEN)) {
    creds.removeCredentials(cJOB_TOKEN);
  }
}

//______________________________________________________________________________
int TJAlienConnectionManager::CreateConnection() {
  gDebug = std::getenv("gDebug") ? std::stoi(std::getenv("gDebug")) : 0;
  TJAlienCredentialsObject co;
  TJAlienDNSResolver dns_jcentral(default_server, default_WSport);
  std::string current_host;

  clearFlags();

  creds.loadCredentials();
  if (creds.count() == 0) {
    ERROR("Failed to get any credentials");
    return -1;
  }

  while (creds.count() > 0) {
    co = creds.get();
    if (!co.exists()) {
      ERROR("Failed to get any credentials");
      return -1;
    }

    if (co.kind == cJBOX_TOKEN || co.kind == cJOB_TOKEN) {
      ConnectJBox(co);
    }

    if (connection_flag) {
      INFO("Successfully connected to JBox");
      co.password = "";
      fWSHost = "localhost";
      return 0;
    }

    std::cout << "Opening connection to JCentral. Please wait" << std::flush;
    for (int i = 0; i < dns_jcentral.length(); i++) {
      std::cout << "." << std::flush;
      current_host = dns_jcentral.get_next_addr();
      ConnectJCentral(co, default_server, current_host);

      if (connection_flag) {
        INFO("Successfully connected to " + current_host);
        co.password = "";
        fWSHost = default_server;
        if (co.kind == cFULL_GRID_CERT)
          return 1;
        else
          return 2;
      } else {
        if (gDebug > 0) {
          std::cout << "\r" << std::flush;
          ERROR("Failed to connect to " + current_host + " - retrying...");
        }
        sleep(1);
      }
    }
    creds.removeCredentials(co.kind);
  }
  ERROR("Failed to connect to any server! Giving up");
  return -1;
}

void TJAlienConnectionManager::clearFlags() {
  destroy_flag = 0;
  connection_flag = 0;
  writeable_flag = 0;
  receive_flag = 0;
  readBuffer = "";
}

//______________________________________________________________________________
void TJAlienConnectionManager::ConnectJBox(TJAlienCredentialsObject c) {
  // Try to load host and port env vars
  std::string jboxHost = std::getenv("JALIEN_HOST") ?: "";
  int jboxPort = std::getenv("JALIEN_WSPORT")
                     ? std::stoi(std::getenv("JALIEN_WSPORT"))
                     : 0;

  if (jboxHost.length() == 0 || jboxPort == 0) {
    // Try to find jclient_token file
    TJClientFile jcf;
    if (jcf.isValid) {
      jboxHost = jcf.fHost;
      jboxPort = jcf.fWSPort;
    } else if (gDebug >= 1)
      INFO("The JClient file is not valid - not connecting to JBox!");
  }

  if (jboxHost.length() == 0 || jboxPort == 0) {
    if (gDebug >= 1)
      INFO("Failed to find any local JBox endpoint");
    return;
  }

  TJAlienDNSResolver dns_jbox(jboxHost, jboxPort);
  MakeWebsocketConnection(c, jboxHost, dns_jbox.get_next_addr(), jboxPort);
}

void TJAlienConnectionManager::ConnectJCentral(TJAlienCredentialsObject c,
                                               string host, string address) {
  if (gDebug > 1)
    INFO("Trying to connect to server " + address);
  MakeWebsocketConnection(c, host, address, default_WSport);
}

//______________________________________________________________________________
void TJAlienConnectionManager::MakeWebsocketConnection(
    TJAlienCredentialsObject creds, string host, string address, int WSPort) {
  // Create the connection to JBox using the parameters read from the token
  // returns true if the connection was established
  bool is_ipv4 = host.find(".") != -1;

  if (gDebug > 0) {
    INFO("Connecting to Server " + host + ":" << WSPort);
    INFO("Using cert " + creds.certpath + " and " + creds.keypath);
  }

  // Use this for debugging
  if (gDebug > 100)
    lws_set_log_level(LLL_EXT | LLL_USER | LLL_PARSER | LLL_INFO | LLL_ERR |
                          LLL_NOTICE,
                      nullptr);
  else
    lws_set_log_level(gDebug > 0 ? 2047 : 0, nullptr);

  // Reset context variables
  context = nullptr;
  wsi = nullptr;

  clearFlags();

  // libwebsockets variables
  struct lws_client_connect_info connect_info;
  struct lws_context_creation_info
      creation_info; // Info to create logical connection
  memset(&connect_info, 0, sizeof connect_info);
  memset(&creation_info, 0, sizeof creation_info);

  // SSL options
  int use_ssl = LCCSCF_USE_SSL; // SSL, do not allow selfsigned certs, check
                                // server hostname

  // Define protocol
  static const struct lws_protocols protocols[] = {
      {"jalien-protocol", ws_service_callback, 0, 0, 1, nullptr},
      {nullptr, nullptr, 0, 0, 0, nullptr} /* end */
  };

  // Create the websockets context. This tracks open connections and
  // knows how to route any traffic and which protocol version to use,
  // and if each connection is client or server side.
  creation_info.port = CONTEXT_PORT_NO_LISTEN; // NO_LISTEN - we are client
  creation_info.iface = nullptr;
  creation_info.protocols = protocols;
  creation_info.extensions = nullptr;
  creation_info.gid = -1;
  creation_info.uid = -1;
  creation_info.options = 0;
  creation_info.vhost_name = "tjalien-root";
  creation_info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
  if (is_ipv4)
    creation_info.options |= LWS_SERVER_OPTION_DISABLE_IPV6;
  creation_info.ws_ping_pong_interval =
      std::getenv("JALIEN_PING_INTERVAL")
          ? std::stoi(std::getenv("JALIEN_PING_INTERVAL"))
          : default_ping_interval;
  creation_info.timeout_secs =
      std::getenv("JALIEN_PING_TIMEOUT")
          ? std::stoi(std::getenv("JALIEN_PING_TIMEOUT"))
          : default_ping_timeout;

  // Create context - only logical connection, no real connection yet
  context = lws_create_context(&creation_info);
  if (context == nullptr) {
    ERROR("Context creation failure");
    destroy_flag = 1;
    return;
  }
  if (gDebug > 1)
    INFO("context created");

  connect_info.address = address.c_str();
  connect_info.port = WSPort;
  connect_info.path = "/websocket/json";
  connect_info.context = context;
  connect_info.ssl_connection = use_ssl;
  connect_info.host = host.c_str();
  connect_info.origin = host.c_str();
  connect_info.ietf_version_or_minus_one = -1;
  connect_info.protocol = protocols[0].name;
  connect_info.pwsi = &wsi;

  // Create wsi - WebSocket Instance
  lws_client_connect_via_info(&connect_info);
  if (wsi == nullptr) {
    if (gDebug > 0)
      ERROR("WebSocket instance creation error");

    return;
  }

  if (gDebug > 1)
    INFO("WebSocket instance creation successfull");

  // Wait for server responce "connection established"
  while (!connection_flag) {
    lws_service(context, 500);
    if (destroy_flag) {
      if (gDebug > 1)
        ERROR("Websocket connection failure");
      return;
    }
  }

  if (creds.kind == cJOB_TOKEN) {
    this->creds.removeCredentials(creds.kind);
  }

  creation_info.ssl_private_key_password = "";
  fWSPort = WSPort;
  return;
}

//______________________________________________________________________________
void TJAlienConnectionManager::ForceRestart() {
  // Immediately break previous connection and start a new one with user grid
  // vertificate
  destroy_flag = 1;
  connection_flag = 0;
  if (context)
    lws_context_destroy(context);

  ConnectJCentral(creds.get(cFULL_GRID_CERT));
  if (!IsConnected()) {
    INFO("Failed to establish the connection to the server");
    return;
  }
}

//______________________________________________________________________________
std::string TJAlienConnectionManager::RunCommand(std::string command) {
  gDebug = std::getenv("gDebug") ? std::stoi(std::getenv("gDebug")) : 0;
  if (gDebug > 1)
    INFO("command to be done: " + command);

  readBuffer = "";
  if (!IsConnected()) {
    ERROR("Connection is broken!");
    return nullptr;
  }
  websocket_write_back(wsi, command.c_str(), -1);
  lws_callback_on_writable(wsi);
  while (!receive_flag && !destroy_flag)
    lws_service(context, 500);

  // If connection is broken or closed by server, notify the client
  // Don't try to reconnect, just return empty string
  if (destroy_flag)
    INFO("Connection should be re-established");

  receive_flag = 0;

  return readBuffer;
}

//_____________________________________________________________________________
//
// Websockets-related part
//_____________________________________________________________________________
size_t WriteCallback(void *contents, size_t size, size_t nmemb) {
  size_t realsize = size * nmemb;
  readBuffer.append((const char *)contents, realsize);
  return realsize;
}

//_____________________________________________________________________________
int websocket_write_back(struct lws *wsi_in, const char *str, int str_size_in) {
  if (str == nullptr || wsi_in == nullptr)
    return -1;

  int n;
  int len;
  char *out = nullptr;

  if (str_size_in < 1)
    len = strlen(str);
  else
    len = str_size_in;

  out = (char *)malloc(sizeof(char) * (LWS_SEND_BUFFER_PRE_PADDING + len +
                                       LWS_SEND_BUFFER_POST_PADDING));
  // setup the buffer
  memcpy(out + LWS_SEND_BUFFER_PRE_PADDING, str, len);
  // write out
  n = lws_write(wsi_in, (unsigned char *)out + LWS_SEND_BUFFER_PRE_PADDING, len,
                LWS_WRITE_TEXT);

  // free the buffer
  free(out);

  return n;
}

//_____________________________________________________________________________
int ws_service_callback(struct lws *wsi, enum lws_callback_reasons reason,
                        void *user, void *in, size_t len) {
  // Websocket callback handler
  UNUSED(len);
  switch (reason) {
  case LWS_CALLBACK_CLIENT_ESTABLISHED: {
    if (gDebug > 1)
      INFO("Connect with server success");
    connection_flag = 1;
    break;
  }

  case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: {
    if (gDebug > 1)
      ERROR("Connect with server error");
    destroy_flag = 1;
    connection_flag = 0;
    if (!lws_get_context(wsi))
      lws_context_destroy(lws_get_context(wsi));
    wsi = nullptr;
    break;
  }

  case LWS_CALLBACK_CLIENT_CLOSED: {
    if (gDebug > 1)
      INFO("LWS_CALLBACK_CLIENT_CLOSED");
    destroy_flag = 1;
    connection_flag = 0;
    if (!lws_get_context(wsi))
      lws_context_destroy(lws_get_context(wsi));
    wsi = nullptr;
    break;
  }

  case LWS_CALLBACK_CLIENT_RECEIVE: {
    if (gDebug > 100) {
      INFO("Client received: " + std::string((char *)in));
      INFO(std::setw(4) << (int)len << " (rpp " << std::setw(5)
                        << (int)lws_remaining_packet_payload(wsi) << ", last "
                        << lws_is_final_fragment(wsi));
    }

    readBuffer.append((char *)in);
    if (lws_is_final_fragment(wsi) != 0)
      receive_flag = 1;

    len = 0;
    break;
  }

  case LWS_CALLBACK_CLIENT_WRITEABLE: {
    if (gDebug > 1)
      INFO("On writeable is called");
    writeable_flag = 1;
    break;
  }

#if defined(LWS_OPENSSL_SUPPORT)
  case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS: {
    if (gDebug > 1)
      INFO("LOAD_EXTRA_CLIENT_VERIFY_CERTS is called");

    TJAlienSSLContext ssl_ctx;
    ssl_ctx.SetCertAndKey((SSL_CTX *)user);
    ssl_ctx.SetCAPath((SSL_CTX *)user);
    break;
  }

#endif

  default:
    break;
  }
  return 0;
}

//______________________________________________________________________________
bool TJAlienConnectionManager::IsConnected() const {
  // Poll the connection to get it's status
  lws_service(context, 0);
  return connection_flag;
}
