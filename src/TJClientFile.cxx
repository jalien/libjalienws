#include "TJClientFile.h"
#include <fstream>

static int gDebug = 0;

TJClientFile::TJClientFile(const char *filepath) {
  tmpdir = getTmpdir();

  if (filepath != NULL) {
    this->isValid = loadFile(filepath);
  } else {
    this->isValid = loadFile(getDefaultPath().c_str());
  }
}

std::string TJClientFile::getDefaultPath() {
  char *cUserId = new char[10];
  sprintf(cUserId, "%d", getuid());

  char *jclientFileLocation = new char[100];
  sprintf(jclientFileLocation, "%s/jclient_token_%s", tmpdir.c_str(), cUserId);
  defaultJClientPath = jclientFileLocation;

  delete[] cUserId;
  delete[] jclientFileLocation;

  return defaultJClientPath;
}

std::string TJClientFile::getTmpdir() {
  std::string tmpdir;

  if (getenv("TMPDIR") != NULL)
    tmpdir = getenv("TMPDIR");
  else if (getenv("TMP") != NULL)
    tmpdir = getenv("TMP");
  else if (getenv("TEMP") != NULL)
    tmpdir = getenv("TEMP");
  else
    tmpdir = P_tmpdir;

  return tmpdir;
}

bool TJClientFile::loadFile(const char *filepath) {
  if (filepath == NULL) {
    return false;
  }

  gDebug = std::getenv("gDebug") ? std::stoi(std::getenv("gDebug")) : 0;
  std::ifstream jclientFile(filepath);
  std::string fileLine;

  bool result = true;

  if (jclientFile.is_open()) {
    while (getline(jclientFile, fileLine)) {
      if (gDebug > 1)
        INFO("Token file line: " + fileLine);

      size_t pos = fileLine.find('=');
      std::string sKey = trim(fileLine.substr(0, pos));
      std::string sValue = trim(fileLine.substr(pos + 1));

      if (sKey.length() != 0 && sValue.length() != 0) {
        if (gDebug > 1)
          INFO("\"" + sKey + "\" = \"" + sValue + "\"");

        if (!sKey.compare("JALIEN_HOST")) {
          fHost = sValue;

          if (fHost.length() == 0) {
            ERROR("JAliEn connection host field empty");
            result = false;
            break;
          }
        }

        if (!sKey.compare("JALIEN_PORT")) {
          fPort = std::stoi(sValue.c_str());
        }

        if (!sKey.compare("JALIEN_WSPORT")) {
          fWSPort = std::stoi(sValue.c_str());

          if (fWSPort == 0) {
            ERROR("JAliEn connection port field empty or misspelled");
            result = false;
            break;
          }
        }

        if (!sKey.compare("JALIEN_HOME")) {
          fHome = sValue;
        }

        if (!sKey.compare("JALIEN_USER")) {
          fUser = sValue;
        }

        if (!sKey.compare("JALIEN_PASSWORD")) {
          fPw = sValue;
        }
      } else {
        if (gDebug > 1)
          ERROR("jclient file does not have the correct structure");
        result = false;
      }
    }

    jclientFile.close();
  } else {
    if (gDebug > 1)
      ERROR("Error while opening jclient file");
    result = false;
  }

  return result;
}
